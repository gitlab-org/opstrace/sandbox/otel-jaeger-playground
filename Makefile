.PHONY: compose
compose:
	docker compose up

.PHONY: restart
restart:
	docker compose restart

.PHONY: tracegen
# example tracegen command to generate traces
tracegen:
	go run github.com/open-telemetry/opentelemetry-collector-contrib/cmd/telemetrygen@v0.85.0 traces --otlp-insecure --duration 5s