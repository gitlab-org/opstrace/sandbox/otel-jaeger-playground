# OpenTelemetry and Jaeger Playground

A simple docker-compose project to allow local experimentation with:

- OpenTelemetry collector contrib image that contains all extra receivers, processors and exporters (see https://github.com/open-telemetry/opentelemetry-collector-contrib).
- Jaeger All-In-One as an exporter for traces and as a UI.
- Prometheus to store Jaeger span metrics for [SPM](https://www.jaegertracing.io/docs/1.41/spm/).

The SPM setup is inspired by https://github.com/jaegertracing/jaeger/tree/main/docker-compose/monitor.

Dependencies:

- Docker with v2 enabled compose - https://docs.docker.com/compose/compose-v2/
- Go >1.20 if you want to run the trace generator.

Use `make compose` to bring up the services. There is no persistent storage, use `make restart` to recreate the containers.

Access Jaeger UI on `http://localhost:16686`.

Use `make tracegen` to generate some traces.